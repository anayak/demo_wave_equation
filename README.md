# Demo FEM solver for Wave Equation

This is a FEM Solver for Wave equation developed using FEniCSx (v0.4.1) with a provided `Makefile` to run the solver for different time-steps and plot errors to indicate convergence. 

The `MakeFile` provides options for the spatial resolution and also time-steps (dt) and is run using `make time_convergence`. Adjust the plotting options in `plot_time_convergence.py`.

> This repo is used in the [Containers talk](https://gitlab.mpi-magdeburg.mpg.de/talk-collection/2022/disc-rg-containers) to showcase the usability of Apptainer `app`s features. 