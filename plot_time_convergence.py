import numpy as np
import pandas as pd
import plotly.graph_objects as go
from argparse import ArgumentParser
import sys
# -------------------------------------------
if len(sys.argv) > 1:
    args = sys.argv[1:]
else :
    print('No arguments provided.')
    print('Set --mres [mesh resolution]  --dt [time step] for control.')
    print('Using default values : ') 
    print('    --mres 32 --dt 0.01')

parser = ArgumentParser()
parser.add_argument(
    '--mres', 
    nargs='?', default=32, type=int,
    help='Mesh Resolution (int)'
)
parser.add_argument(
    '--dt',
    nargs='+', default='1e-2', type=float,
    help='Time Step (float)'
)
args = parser.parse_args()
# -------------------------------------------

file_base = 'wave_eqn_linear' 
mRES = args.mres #['32']
dt_list = args.dt #['1.00e-01', '1.00e-02', '1.00e-03']

t_l2errors = []
for dt in dt_list:
    # Read errors
    data_t = pd.read_csv(f'{file_base}_mres{mRES:d}_dt{dt:.2e}.errors', index_col=0)

    # Find l2 error in t
    t_errors  = data_t.iloc[:,1].values
    t_l2errors.append(np.sqrt(np.sum(np.square(t_errors)) * float(dt)))

# Fit line
dt_list = np.array(dt_list).astype(float)
t_l2errors = np.array(t_l2errors)
t_fitline = np.polyfit(np.log10(dt_list), np.log10(t_l2errors), deg=1)


# Plot
fig = go.Figure()
fig.add_scatter(
    x=dt_list, y=t_l2errors,
    name=f'Fitted slope: {t_fitline[0]:.2f}',
    mode='markers+lines',
    marker={'size': 8},
)
fig.update_layout(
    xaxis_title='Time Step, dt',
    yaxis_title='L2 Errors',
    xaxis={'type':'log',},
    yaxis={'type':'log', 'tickformat': '.2e'},
    font={'family': 'Latin Modern Math', 'size': 24},
    showlegend=True
)
fig.write_image(file_base+'_time_convergence.png', height=720, width=1280)
