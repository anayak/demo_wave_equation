# 1D Wave Equation Solver in Time domain
# 
# -------------------------------------------
# Import
import sys
from argparse import ArgumentParser
import numpy as np

import ufl
from dolfinx import mesh, fem, io

from mpi4py import MPI
from petsc4py import PETSc

import pandas as pd
import plotly.express as px
# -------------------------------------------
if len(sys.argv) > 1:
    args = sys.argv[1:]
else :
    print('No arguments provided.')
    print('Set --mres [mesh resolution]  --dt [time step] for control.')
    print('Using default values : ') 
    print('    --mres 32 --dt 0.01')

parser = ArgumentParser()
parser.add_argument(
    '--mres', 
    nargs='?', default=32, type=int,
    help='Mesh Resolution (int)'
)
parser.add_argument(
    '--dt',
    nargs='?', default='1e-2', type=float,
    help='Time Step (float)'
)
args = parser.parse_args()
# -------------------------------------------
# PARAMETERS
# Time controls
t_0 = 0.00  # Start Time
t_f = 1.00  # End Time
dt = args.dt

niters = np.ceil((t_f - t_0) / dt)
iter = 0

# Space controls
Lpipe = 1.0
Lx = (0.0, Lpipe)

# Coeffs for Spatial derivative scheme
alpha = 1.0
beta = 1.0
gamma = 1.0

# Problem config
a = 1.0

# Test case config
T = (t_f-t_0)   # Solution period

RES = args.mres

# -------------------------------------------
# Create 1D Mesh
pipe_mesh = mesh.create_interval(MPI.COMM_WORLD, RES, [0.0, Lpipe])

# Boundaries
node_dim = pipe_mesh.topology.dim - 1
inlet_node = mesh.locate_entities(pipe_mesh, node_dim, lambda x: np.isclose(x[0], 0.0))
outlet_node = mesh.locate_entities(pipe_mesh, node_dim, lambda x: np.isclose(x[0], Lpipe))

# PDE setup
V = fem.FunctionSpace(pipe_mesh, ('Lagrange', 1))
dx = ufl.Measure('dx', domain=pipe_mesh)
# ds = ufl.Measure('ds', subdomain_data=boundaries, domain=mesh)

# Expressions for space-varying functions
class Phi_expr:
    """ Exact solution - spatially varying and time dependent """
    def __init__(self):
        self.t = 0.0
    def eval(self, x):
        return np.sin(self.t) * np.cos(x[0])


# instantiate expression classes
Phi_exp = Phi_expr()

# Functions
# unknown and test functions
Phi = ufl.TrialFunction(V)
# Phi.x.array[:] = 0.0
Psi = ufl.TestFunction(V)

# exact solution
Phi_exp.t = t_0
Phi_Exact = fem.Function(V)
Phi_Exact.interpolate(Phi_exp.eval) 

# error function
Error_Phi = fem.Function(V)
# Error_Phi.x.array[:] = Phi.x.array[:] - Phi_Exact.x.array[:]

# Assemble PDE variational form
# initial condition

# at time n 
t = t_0
Phi_n = fem.Function(V)
Phi_n.interpolate(Phi_exp.eval)


# at time n-1
Phi_exp.t = t_0-dt
Phi_n_1 = fem.Function(V)
Phi_n_1.interpolate(Phi_exp.eval)

# form
A = (
    Phi/(dt**2) * Psi * dx
    + a**2 * ufl.Dx(alpha*Phi, 0) / (alpha+beta+gamma) * ufl.Dx(Psi, 0) * dx
    )
b = (
    (2*Phi_n - Phi_n_1)/(dt**2) * Psi * dx
    - a**2 * ufl.Dx(beta*Phi_n + gamma*Phi_n_1, 0) / (alpha+beta+gamma) * ufl.Dx(Psi, 0) * dx 
)

# boundary conditions
bcs = [ 
    fem.dirichletbc(Phi_Exact, inlet_node),
    fem.dirichletbc(Phi_Exact, outlet_node)
]

# Time loop
# Phi.name = 'Phi'
Phi_Exact.name = 'Phi_Exact'
Error_Phi.name = 'Error_Phi'
iofile = io.XDMFFile(MPI.COMM_WORLD, f'results_m{RES}_dt{dt:.2e}.xdmf', 'w')
iofile.write_mesh(pipe_mesh)
iofile.write_function(Phi_n, t_0)
iofile.write_function(Phi_Exact, t_0)
iofile.write_function(Error_Phi, t_0)

time_list = []
L2errors = []
Phi_h = fem.Function(V)
print(f'CFL = {a*dt*RES/Lpipe}')
for iter in np.arange(niters) :
    # Update Time
    t = t + dt
    iter = iter + 1
    print(f"[*] Solving for time {t:.2f}, step {iter}")

    # Update expressions, functions and boundary conditions
    Phi_exp.t = t
    Phi_Exact.interpolate(Phi_exp.eval)
    for i, bc in enumerate(bcs):
        fem.set_bc([Phi_exp.eval(x=(Lx[i],))], [bc])

    # # Solve PDE
    problem = fem.petsc.LinearProblem(A, b, bcs)
    Phi_h = problem.solve()

    # Reassign functions
    Phi_n_1.x.array[:] = Phi_n.x.array[:]
    Phi_n.x.array[:] = Phi_h.x.array[:]

    # Errors w.r.to Exact solution
    Error_Phi.x.array[:] = Phi_h.x.array[:] - Phi_Exact.x.array[:]
    Error_Norm = np.sqrt(fem.assemble_scalar(fem.form(Error_Phi*Error_Phi*dx)))
    print(f'[ ] L2 Error : {Error_Norm}')
    
    Phi_h.name='Phi'
    iofile.write_function(Phi_h, t)
    iofile.write_function(Phi_Exact, t)
    iofile.write_function(Error_Phi, t)

    L2errors.append(Error_Norm)
    time_list.append(t)

iofile.close()
# Output Errors
df = pd.DataFrame(list(zip(time_list, L2errors)), columns=['t', 'L2errors'])
df.to_csv(f'wave_eqn_linear_mres{RES}_dt{dt:0.2e}.errors')

fig = px.line(df, x='t', y='L2errors')
fig.update_layout(font={'size':24, 'family': 'Latin Modern Math'})
fig.write_image(f'wave_eqn_linear_mres{RES}_dt{dt:0.2e}.png', height=720, width=1280)