DEMO_PATH=/demo_wave_equation
M_RES=32
DT_LIST=1.00e-01 1.00e-02 1.00e-03

.PHONY: time_convergence
time_convergence: wave_equation
	python3 ${DEMO_PATH}/plot_time_convergence.py --mres ${M_RES} --dt ${DT_LIST}

wave_equation:
	for DT in ${DT_LIST} ; do \
	  python3 ${DEMO_PATH}/wave_equation_solver.py --mres ${M_RES} --dt $${DT}; \
	done
	

.PHONY: clean
clean:
	rm -rf *.errors *.png *.h5 *.xdmf
